<?php

/**
 * Unit tests for Tiler class
 *
 * @author Damian
 */
require_once 'Tiler.php';

class TilerTest extends PHPUnit_Framework_TestCase {

    public function testCalculateNumberOfTiles() {
        $tiler = new Tiler(new Room(4, 5), new Tile(0.2, 0.25));
        $result = $tiler->calculateNumberOfTiles();
        $expected = 400;

        $this->assertEquals($expected, $result);
    }

    public function testCalculateNumberOfTiles2() {
        try {
            $tiler = new Tiler(new Room(3, 4), new Tile(null, false));
            $tiler->calculateNumberOfTiles();
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), "Input must be a number.");
        }
    }
    
    public function testCalculateNumberOfTiles3() {
        try {
            $tiler = new Tiler(new Room("3m", "4m"), new Tile(0.2, 0.2));
            $tiler->calculateNumberOfTiles();
        } catch (Exception $e) {
            $this->assertEquals($e->getMessage(), "Input must be a number.");
        }
    }

}
