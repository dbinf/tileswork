<?php

/**
 * Class represents a tiler which calculates numbers
 * of tiles needed a floor to be tiled
 *
 * @author Damian
 */
require_once 'Room.php';
require_once 'Tile.php';

class Tiler {

    private $room;
    private $tile;
    private $tilesNumber;
    private $halfTileWidthLeft;
    private $halfTileLengthLeft;

    public function __construct(Room $room, Tile $tile) {
        $this->room = $room;
        $this->tile = $tile;
        $this->tilesNumber = 0;
        $this->halfTileWidthLeft = FALSE;
        $this->halfTileLengthLeft = FALSE;
    }

    /**
     * @return integer number of tiles needed for tiling a room
     */
    public function calculateNumberOfTiles() {
        $perWidth = $this->tilesNumberPerRoomWidth();
        $perLength = $this->tilesNumberPerRoomLength();
        $this->tilesNumber = $perWidth * $perLength; //number of tiles which doesnt have to be cut

        $roomW = $this->room->getRoomWidth();
        $tileW = $this->tile->getTileWidth();
        $widthLeft = $roomW - $tileW * $perWidth;
        if ($widthLeft > $tileW / 2) {
            $this->tilesNumber += $perLength;
        } elseif ($widthLeft != 0) {
            $this->tilesNumber += (int) round($perLength / 2);
            $this->halfTileWidthLeft = !$this->isEven($perLength);
        }

        $roomL = $this->room->getRoomLength();
        $tileL = $this->tile->getTileLength();
        $lengthLeft = $roomL - $tileL * $perLength;
        if ($lengthLeft > $tileL / 2) {
            $this->tilesNumber += $perWidth;
        } elseif ($lengthLeft != 0) {
            $this->tilesNumber += (int) round($perWidth / 2);
            $this->halfTileLengthLeft = !$this->isEven($perWidth);
        }

        if (!($this->halfTileWidthLeft || $this->halfTileLengthLeft) && $widthLeft != 0 && $lengthLeft != 0) {
            $this->tilesNumber++;
        }

        return $this->tilesNumber;
    }

    private function tilesNumberPerRoomWidth() {
        $roomW = $this->room->getRoomWidth();
        $tileW = $this->tile->getTileWidth();

        return (int) ($roomW / $tileW);
    }

    private function tilesNumberPerRoomLength() {
        $roomL = $this->room->getRoomLength();
        $tileL = $this->tile->getTileLength();

        return (int) ($roomL / $tileL);
    }

    private function isEven($numb) {
        return ($numb % 2 == 0);
    }

}
