<?php

require_once 'Tiler.php';

echo "Tileswork - solution validation" . PHP_EOL;

$validationData = array(
    array(4, 5, 0.2, 0.25),
    array(4, 5, 0.3, 0.3),
    array(10, 10, 0.8, 0.8),
    array(10, 10, 0, 5),
    array(0, 10, 0.1, 0.1),
    array(10, 10, -0.5, 0.5),
    array(-3, 4, 0.1, 0.1),
    array("3m", "4m", 0.1, 0.1),
    array(3, 4, null, false)
);

foreach ($validationData as $data => $elem) {
    try {
        $tiler = new Tiler(new Room($elem[0], $elem[1]), new Tile($elem[2], $elem[3]));
        echo "Number of tiles: " . $tiler->calculateNumberOfTiles() . PHP_EOL;
    } catch (Exception $e) {
        echo 'Caught exception: ', $e->getMessage() . PHP_EOL;
    }
}
