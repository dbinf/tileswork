<?php

/**
 * Class represents a room to tiling
 *
 * @author Damian
 */
require_once 'inputBaseValidator.php';

class Room {

    private $roomWidth;
    private $roomLength;

    public function __construct($roomWidth, $roomLength) {
        $validator = new inputBaseValidator();
        $validator->isInputNumeric($roomWidth);
        $validator->isInputPositiveNumber($roomWidth);
        $validator->isInputNumeric($roomLength);
        $validator->isInputPositiveNumber($roomLength);

        $this->roomWidth = $roomWidth;
        $this->roomLength = $roomLength;
    }

    public function getRoomWidth() {
        return $this->roomWidth;
    }

    public function getRoomLength() {
        return $this->roomLength;
    }

}
