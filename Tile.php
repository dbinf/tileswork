<?php

/**
 * Class represents a tile 
 *
 * @author Damian
 */
require_once 'inputBaseValidator.php';

class Tile {

    private $tileWidth;
    private $tileLength;

    public function __construct($tileWidth, $tileLength) {
        $validator = new inputBaseValidator();
        $validator->isInputNumeric($tileWidth);
        $validator->isInputPositiveNumber($tileWidth);
        $validator->isInputNumeric($tileLength);
        $validator->isInputPositiveNumber($tileLength);
        
        $this->tileWidth = $tileWidth;
        $this->tileLength = $tileLength;
    }

    public function getTileWidth() {
        return $this->tileWidth;
    }

    public function getTileLength() {
        return $this->tileLength;
    }

}
