<?php

/**
 * Class represents a validator object.
 * 
 * This class includes basic validation methods. In more complex examples it might be used
 * as a parent class for other validators. It shouldnt be bult as a master validation object 
 * with thousends of methods.
 *
 * Another solution might be an interface which should be implemented by given class.
 * 
 * Both solutions are not perfect but thanks to them we achive e.g lower level o code redundancy.
 * 
 * @author Damian
 */
class inputBaseValidator {

    public function isInputNumeric($input) {
        if (!is_numeric($input)) {
            throw new Exception('Input must be a number.');
        }

        return true;
    }

    public function isInputPositiveNumber($input) {
        if ($input <= 0) {
            throw new Exception('Input cant be equal or lower than 0.');
        }

        return true;
    }

}
